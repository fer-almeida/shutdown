#include <stdio.h>
#include <string.h>

#include "version.h"

#define LANGUAGE	ENGLISH
#include "res.h"

void shutdown_pc(void) {
	asm {
		mov ax, 0x5301
		xor bx, bx
		int 0x15

		mov ax, 0x530E
		xor bx, bx
		mov cx, 0x0102
		int 0x15

		mov ax, 0x5307
		mov bx, 0x0001
		mov cx, 0x0003
		int 0x15

		cli;
		hlt;
	}
}

void help_message(void) {
	printf("%s", HELP_MSG);
}

void version_message(void) {
	printf("%s - %s v%d.%02d\r\n", PRODUCT, COMMAND, MAJOR_VERSION, MINOR_VERSION);
}

int main(int argc, char *argv[]) {
	if(strstr(argv[0], COMMAND) == NULL) {
		printf("%s\r\n", ERRORCMD_MSG);
		return -1;
	}

	if(argc == 1) {
		printf("%s\r\n", SHUTDOWN_MSG);
		shutdown_pc();
	} else {
		if(strcmp(argv[1], "/?") == 0) { help_message(); return 0; }
		if(strcmp(argv[1], "/v") == 0 || strcmp(argv[1], "/V") == 0) { version_message(); return 0; }
		if(strcmp(argv[1], "/s") == 0 || strcmp(argv[1], "/S") == 0) { shutdown_pc(); return 0; }

		printf("%s - %s\r\n", INVALIDSW_MSG, argv[1]);
	}
	return 0;
}