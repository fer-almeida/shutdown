#ifndef _RES_H
#define _RES_H

// English
#define ENGLISH                 1
#define EN_SHUTDOWN_MSG		"Shutting down PC..."
#define EN_HELP_MSG		"Shutdown the computer.\r\n\r\nSHUTDOWN [/V] [/S]\r\n\r\n  /V      Show version information.\r\n  /S      Silent mode - Does not show shutting down message.\r\n"
#define EN_INVALIDSW_MSG	"Invalid switch"
#define EN_ERRORCMD_MSG		"Bad command or file name"

// Portuguese
#define PORTUGUESE              2
#define PT_SHUTDOWN_MSG		"Desligando o PC..."
#define PT_HELP_MSG             "Desliga o computador.\r\n\r\nSHUTDOWN [/V] [/S]\r\n\r\n  /V      Exibe informacoes da versao.\r\n  /S      Modo silencioso - Nao exibe a mensagem ao desligar.\r\n"
#define PT_INVALIDSW_MSG        "Parametro invalido"
#define PT_ERRORCMD_MSG         "Comando ou nome de arquivo invalido"

// Spanish
#define SPANISH                 3
#define SP_SHUTDOWN_MSG		"Apagando el PC..."
#define SP_HELP_MSG             "Apaga el equipo.\r\n\r\nSHUTDOWN [/V] [/S]\r\n\r\n  /V      Mostrar informacion de version.\r\n  /S      Modo silencioso - No muestra el mensage al apagar.\r\n"
#define SP_INVALIDSW_MSG        "Parametro invalido"
#define SP_ERRORCMD_MSG         "Comando o nombre de archivo erroneo"

// French
#define FRENCH                  4
#define FR_SHUTDOWN_MSG		"Arreter le PC..."
#define FR_HELP_MSG             "Eteindre l'ordinateur.\r\n\r\nSHUTDOWN [/V] [/S]\r\n\r\n  /V      Afficher les informations de version.\r\n  /S      Mode silencieux - Ne mostre pas le message de arret.\r\n"
#define FR_INVALIDSW_MSG        "Parametre invalide"
#define FR_ERRORCMD_MSG         "Mauvaise commande ou nom de fichier"

// German
#define GERMAN			5
#define GR_SHUTDOWN_MSG		"Das Herunterfahren PC..."
#define GR_HELP_MSG             "Schalten Sie Ihren Computer aus.\r\n\r\nSHUTDOWN [/V] [/S]\r\n\r\n  /V      Versionsinformationen anzeigen.\r\n  /S      Hudemodus - Zeigt keine Meldung zum Herunterfahren an.\r\n"
#define GR_INVALIDSW_MSG        "Ungultiger Parameter"
#define GR_ERRORCMD_MSG         "Fehlerhafter Befel oder Dateiname"

// String Table
#if   LANGUAGE == ENGLISH
	#define SHUTDOWN_MSG	EN_SHUTDOWN_MSG
	#define HELP_MSG        EN_HELP_MSG
	#define INVALIDSW_MSG   EN_INVALIDSW_MSG
	#define ERRORCMD_MSG    EN_ERRORCMD_MSG
#elif LANGUAGE == PORTUGUESE
	#define SHUTDOWN_MSG	PT_SHUTDOWN_MSG
	#define HELP_MSG        PT_HELP_MSG
	#define INVALIDSW_MSG   PT_INVALIDSW_MSG
	#define ERRORCMD_MSG    PT_ERRORCMD_MSG
#elif LANGUAGE == SPANISH
	#define SHUTDOWN_MSG	SP_SHUTDOWN_MSG
	#define HELP_MSG        SP_HELP_MSG
	#define INVALIDSW_MSG   SP_INVALIDSW_MSG
	#define ERRORCMD_MSG    SP_ERRORCMD_MSG
#elif LANGUAGE == FRENCH
	#define SHUTDOWN_MSG	FR_SHUTDOWN_MSG
	#define HELP_MSG        FR_HELP_MSG
	#define INVALIDSW_MSG   FR_INVALIDSW_MSG
	#define ERRORCMD_MSG    FR_ERRORCMD_MSG
#elif LANGUAGE == GERMAN
	#define SHUTDOWN_MSG	GR_SHUTDOWN_MSG
	#define HELP_MSG        GR_HELP_MSG
	#define INVALIDSW_MSG   GR_INVALIDSW_MSG
	#define ERRORCMD_MSG    GR_ERRORCMD_MSG
#endif

#endif
